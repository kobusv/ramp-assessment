import pandas as pd

ORIGINAL_DEFAULTS = [
    [1, 1, 1, 1, 1, 1, 1],
    [2, 2, 2, 2, 2, 2, 2],
    [3, 3, 3, 3, 3, 3, 3],
]


def calculate_modified(data, original=ORIGINAL_DEFAULTS):
    original_df = pd.DataFrame(original)
    input_df = pd.DataFrame([data] * 3)
    multiplied = original_df.multiply(input_df)

    return [list(i) for i in multiplied.round(3).values]


def matrix_to_string(matrix):
    return "\n".join([",".join([str(x) for x in i]) for i in matrix])


def pretty_print_modified_weekly_results(data, original=ORIGINAL_DEFAULTS):
    formatted_original = matrix_to_string(original)
    formatted_data = matrix_to_string(data)
    return (
        f"Your original weekly results for the next three weeks are:\n{formatted_original}\n"
        f"Your modified weekly results for the next three weeks are:\n{formatted_data}"
    )


def validate_and_parse_weekly_data_input(data):
    if not data:
        return False, "No input data provided"

    try:
        parsed = [float(i) for i in data.split(",")]
    except ValueError:
        return False, "All values need to be decimal values"

    if len(parsed) != 7:
        return False, "Input must be a comma separated list of 7 decimal values"

    return True, parsed
