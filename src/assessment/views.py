from flask import request, make_response
from assessment import app
from assessment.logic import (
    calculate_modified,
    pretty_print_modified_weekly_results,
    validate_and_parse_weekly_data_input,
)


@app.route("/", methods=["GET", "POST"])
def calculate_modified_weekly_results(**kwargs):
    data = ""
    if request.method == "POST" and request.data:
        data = request.data.decode("UTF-8")

    if request.method == "GET":
        data = request.args.get("input", None)

    valid, parsed = validate_and_parse_weekly_data_input(data)

    if not valid:
        return make_response(parsed, 400)

    try:
        modified = calculate_modified(parsed)
        return make_response(pretty_print_modified_weekly_results(modified), 200)
    except Exception as e:
        app.logger.error(e)
        return make_response("Something catastrophic happened", 500)
