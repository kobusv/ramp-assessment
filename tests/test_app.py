from assessment import app
from assessment.logic import (
    calculate_modified,
    matrix_to_string,
    ORIGINAL_DEFAULTS,
)

app.testing = True
client = app.test_client()

EXPECTED_RESPONSE_DATA = (
    b"Your original weekly results for the next three weeks are:\n"
    b"1,1,1,1,1,1,1\n"
    b"2,2,2,2,2,2,2\n"
    b"3,3,3,3,3,3,3\n"
    b"Your modified weekly results for the next three weeks are:\n"
    b"0.1,0.9,0.123,0.99,0.5,1.0,0.0\n"
    b"0.2,1.8,0.246,1.98,1.0,2.0,0.0\n"
    b"0.3,2.7,0.369,2.97,1.5,3.0,0.0"
)


def test_modified_weekly_results_api_get():
    response = client.get("/?input=0.1,0.9,0.123,0.99,0.5,1.0,0")

    assert EXPECTED_RESPONSE_DATA in response.data
    assert 200 == response.status_code


def test_modified_weekly_results_api_post():
    response = client.post("/", data="0.1,0.9,0.123,0.99,0.5,1.0,0")

    assert EXPECTED_RESPONSE_DATA in response.data
    assert 200 == response.status_code


def test_invalid_input_data_type():
    response = client.get("/?input=0.1,bar,0.123,foo,0.5,1.0,0")

    assert 400 == response.status_code
    assert b"All values need to be decimal values" == response.data


def test_invalid_input_data_length():
    response = client.get("/?input=0.1,0.9,0.123,0.99,0.5")
    expected = b"Input must be a comma separated list of 7 decimal values"

    assert 400 == response.status_code
    assert expected == response.data


def test_calculation():
    expected = [
        [0.1, 0.9, 0.123, 0.99, 0.5, 1.0, 0.0],
        [0.2, 1.8, 0.246, 1.98, 1.0, 2.0, 0.0],
        [0.3, 2.7, 0.369, 2.97, 1.5, 3.0, 0.0],
    ]

    assert calculate_modified([0.1, 0.9, 0.123, 0.99, 0.5, 1.0, 0]) == expected


def test_matrix_to_string():
    sample = [
        [0.1, 0.9, 0.123, 0.99, 0.5, 1.0, 0.0],
        [0.2, 1.8, 0.246, 1.98, 1.0, 2.0, 0.0],
        [0.3, 2.7, 0.369, 2.97, 1.5, 3.0, 0.0],
    ]
    expected = (
        "0.1,0.9,0.123,0.99,0.5,1.0,0.0\n"
        "0.2,1.8,0.246,1.98,1.0,2.0,0.0\n"
        "0.3,2.7,0.369,2.97,1.5,3.0,0.0"
    )
    expected_default = "1,1,1,1,1,1,1\n2,2,2,2,2,2,2\n3,3,3,3,3,3,3"

    assert matrix_to_string(sample) == expected
    assert matrix_to_string(ORIGINAL_DEFAULTS) == expected_default
