# Ramp Assessment

This is my submission for the Ramp technical assessment.

## Requirements

You need to have python3.8 and poetry installed

## Getting started

- Clone this project:

```sh
git clone git@gitlab.com:kobusv/ramp-assessment.git
```

- Change into the directory:

```sh
cd ramp-assessment/
```

- Install the dependencies using with the following command:

```sh
poetry install
```

- Start the development server using the following command

```sh
poetry run flask run
```

The API should now be accessible on `http://127.0.0.1:5000/`

## Automated tests

Tests are stored in the `tests` directory. Run the tests using `pytest` with the following command:

```sh
poetry run pytest
```

## Sending request

To test the API you can use `curl` or another client like Postman.
Send requests to `http://localhost:5000/`. The API accepts both `POST` and `GET` requests

Example using `POST`:

```sh
curl --location --request POST 'http://localhost:5000/' \
--header 'Content-Type: text/plain' \
--data-raw '0.1,0.9,0.123,0.99,0.5,1.0,0'
```

Example using `GET`:

```sh
curl --location --request GET 'http://localhost:5000/?input=0.1,0.9,0.123,0.99,0.5,1.0,0'
```

## Assumptions and judgement calls

- Since incoming data might not necessarily be the correct data type (decimal/float) or the correct length (7 comma separated items), I have added some input validation to the API checking both data type and length of the input

- I added some tests to confirm that the API works with both `GET` and `POST` requests and also responds with the suitable errors when input validation fails.

- I assumed that the original weekly results will always be the following (these values are currently hard coded as default values):

```
1,1,1,1,1,1,1
2,2,2,2,2,2,2
3,3,3,3,3,3,3
```

- After calculating the modified weekly results all the values are rounded only to the third decimal. The example provided only had values with up to 3 decimal values.

- I have separated most of the business logic from the API layer to make it less dependant on Flask. This should make it a bit easier to port to another framework like Django in future.
